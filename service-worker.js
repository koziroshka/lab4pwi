const CACHE_NAME = 'lab2-cache-v1';
const urlsToCache = [
    './',
    './index.html',
    './styles.css',
    './header1.css',
    './add-user.png',
    './draw.png',
    './Volodka.jpg',
    './scriptHorse.js',
    './photo_2022-09-01_19-13-47.jpg',
    './photo_2024-02-23_15-22-10.jpg',
    'https://code.jquery.com/jquery-3.6.0.min.js',
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css',
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js'
];

self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                if (response) {
                    return response;
                }
                return fetch(event.request);
            })
    );
});
