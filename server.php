<?php


session_start();


function insertDataIntoDatabase($group, $name, $lastname, $gender, $birthday, $studentID) {


    if (empty($name) || empty($lastname) || empty($gender) || empty($birthday)) {
           http_response_code(400);
           echo json_encode(array("error" => "Please enter all required information."));
           exit;
       }

       // Validate name and lastname: check if they contain only letters
       if (!ctype_alpha($name) || !ctype_alpha($lastname)) {
           http_response_code(400);
           echo json_encode(array("error" => "Invalid name. Name should contain only letters."));
           exit;
       }
    // Validate name: check if it contains only letters
    if (!preg_match("/^[a-zA-Z]+$/", $name) || !preg_match("/^[a-zA-Z]+$/", $lastname)) {
        http_response_code(400);
        echo json_encode(array("error" => "Invalid name. Name should contain only letters."));
        exit;
    }

    $birthday_timestamp = strtotime($birthday);
    $age = (date('Y') - date('Y', $birthday_timestamp));
    if ($age < 18) {
        http_response_code(400);
        echo json_encode(array("error" => "User is not an adult."));
        exit;
    }

    $servername = "127.0.0.1";
        $username = "root";
        $password = "";
        $dbname = "testforlab";

        $conn = new mysqli($servername, $username, $password, $dbname);

        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }



    $sql = "INSERT INTO databasestudent (group_name, first_name, last_name, gender_name, birthday_name) VALUES ('$group', '$name', '$lastname', '$gender', '$birthday')";

    if ($conn->query($sql) === TRUE) {
        echo "Data inserted successfully.";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }

    $conn->close();
}


// Function to fetch all users from the database and return as JSON
function fetchAllUsers() {
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "testforlab";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM databasestudent";
    $result = $conn->query($sql);

    $users = array();

    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $users[] = $row;
        }
    }

    $conn->close();

    return json_encode($users);
}

// Check if the action is to fetch all users
if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["action"]) && $_GET["action"] === "fetchUsers") {
    // Call the function to fetch all users and return as JSON
    echo fetchAllUsers();
}


// Function to fetch the maximum student ID from the database and return as JSON
function fetchMaxStudentID() {
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "testforlab";

    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT MAX(student_id) AS max_id FROM databasestudent";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $maxStudentID = $row['max_id'];
    } else {
        $maxStudentID = 0; // Set default value if no rows found
    }

    $conn->close();

    return json_encode(array("max_student_id" => $maxStudentID));
}

// Check if the action is to fetch the maximum student ID
if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["action"]) && $_GET["action"] === "max_student_id") {
    // Call the function to fetch the maximum student ID and return as JSON
    echo fetchMaxStudentID();
}



// Function to update student information in the database
function updateStudent($group, $name, $lastname, $gender, $birthday, $studentID) {
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "testforlab";
    echo "Shit. " . $studentID;
    $conn = new mysqli($servername, $username, $password, $dbname);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Prepare a SQL statement to update the student record
    $sql = "UPDATE databasestudent SET group_name = ?, first_name = ?, last_name = ?, gender_name = ?,       = ? WHERE student_id = ?";

    // Prepare and bind parameters
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssi", $group, $name, $lastname, $gender, $birthday, $studentID);

    // Execute the statement
    if ($stmt->execute()) {
        // If update is successful
        echo "Student information updated successfully. " . $studentID;
    } else {
        // If an error occurred during update
        echo "Error updating student information: " . $conn->error;
    }

    // Close the statement and connection
    $stmt->close();
    $conn->close();
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["action"]) && $_POST["action"] === "login") {
    // Database connection parameters
    $servername = "127.0.0.1";
    $username = "root";
    $password = "";
    $dbname = "testforlab";

    // Establish database connection
    $pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    if (!$pdo) {
        die("Connection failed: " . $pdo->errorInfo());
    }

    // Validate email and password inputs
    if (isset($_POST["email"]) && isset($_POST["password"])) {
        $email = $_POST['email'];
        $password = $_POST['password'];

        // Output email and password to the error log (console)
        error_log("Email: " . $email);
        error_log("Password: " . $password);

        // Prepare and execute SQL query to retrieve user information
        $sql = "SELECT * FROM databasestudent WHERE email = :email";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        // Fetch user data
        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        // Check if user exists and password matches
        if ($user && $password === $user['password']) {
            // Set session variable for logged-in user
            $_SESSION['username'] = $user['first_name']; // Set first_name as username
            // Return success response with username
            echo json_encode(array('success' => true, 'username' => $user['first_name']));
        } else {
            // Return failure response if user not found or password incorrect
            echo json_encode(array('success' => false, 'message' => 'Invalid email or password'));
        }
    } else {
        // Return failure response if email or password is missing
        echo json_encode(array('success' => false, 'message' => 'Missing email or password'));
    }
}




if ($_SERVER["REQUEST_METHOD"] == "GET" && isset($_GET["action"]) && $_GET["action"] === "findStudentWithID") {
        $servername = "127.0.0.1";
        $username = "root";
        $password = "";
        $dbname = "testforlab";

        // Create connection
        $pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    // Check connection
    if (!$pdo) {
        die("Connection failed: " . $pdo->errorInfo()); // Changed to use $pdo->errorInfo() to get error details
    }

    // Check if student_id is provided in the GET request
    if (isset($_GET["student_id"])) {
        // Sanitize the input to prevent SQL injection
        $student_id = htmlspecialchars($_GET["student_id"]);

        // Prepare the SQL statement to retrieve information about the student with the given ID
        $sql = "SELECT * FROM databasestudent WHERE student_id = :student_id";

        // Prepare the SQL statement
        $stmt = $pdo->prepare($sql);

        // Bind the parameter
        $stmt->bindParam(':student_id', $student_id, PDO::PARAM_INT);

        // Execute the statement
        $stmt->execute();

        // Fetch the result as an associative array
        $student = $stmt->fetch(PDO::FETCH_ASSOC);

        // Check if student exists
        if ($student) {
            // Convert the result to JSON and echo it
            echo json_encode($student);
        } else {
            // Student not found
            echo json_encode(array("message" => "Student not found"));
        }
    } else {
        // student_id parameter is missing
        echo json_encode(array("message" => "Missing student_id parameter"));
    }
}



// Check if the action is to update student information
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["action"]) && $_POST["action"] === "update") {

    // Get the JSON string received from JavaScript
       $jsonData = file_get_contents('php://input');

       // Parse the JSON string to extract the idOfStudent parameter
       parse_str($jsonData, $parsedData);

       // Initialize a variable to store the parsed data
       $postData = $parsedData;

       // Check if idOfStudent exists in the parsed data and assign its value to $Id_student
           if (isset($postData['idOfStudent'])) {
               $Id_student = $postData['idOfStudent'];
               echo "ID of Student: " . $Id_student . "\n";
           } else {
               echo "ID of Student not found." . "\n";
           }

           // Check if group exists in the parsed data and assign its value to $group
           if (isset($postData['group'])) {
               $group = $postData['group'];
               echo "Group: " . $group . "\n";
           } else {
               echo "Group not found." . "\n";
           }

           if (isset($postData['firstName'])) {
                $firstName = $postData['firstName'];
                echo "firstName: " . $firstName . "\n";
                } else
                {
                    echo "firstName not found." . "\n";
                }

           if (isset($postData['lastName'])) {
                          $lastName = $postData['lastName'];
                          echo "lastName: " . $lastName . "\n";
                      } else {
                          echo "lastName not found." . "\n";
                      }


             if (isset($postData['gender'])) {
                                      $gender = $postData['gender'];
                                      echo "gender: " . $gender . "\n";
                                  } else {
                                      echo "gender not found." . "\n";
                                  }


            if (isset($postData['birthday'])) {
                                                  $birthday = $postData['birthday'];
                                                  echo "birthday: " . $birthday . "\n";
                                              } else {
                                                  echo "birthday not found." . "\n";
                                              }



     updateStudent($group, $firstName, $lastName, $gender, $birthday, $Id_student);
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST["action"]) && $_POST["action"] === "add") {
        // Retrieve data from POST parameters
        $group = $_POST["group"];
        $name = $_POST["firstName"];
        $lastname = $_POST["lastName"];
        $gender = $_POST["gender"];
        $birthday = $_POST["birthday"];
        $idOfStudent = $_POST["idOfStudent"];



        // Call the function to insert data into the database
        insertDataIntoDatabase($group, $name, $lastname, $gender, $birthday, $idOfStudent);
    }

    if (isset($_POST["action"]) && $_POST["action"] === "delete") {
        // Check if the student ID is provided
        if (isset($_POST["studentID"])) {
            // Sanitize and validate the student ID
            $studentID = intval($_POST["studentID"]);

            // Connect to your MySQL database
            $servername = "127.0.0.1"; // Replace with your MySQL server address
            $username = "root"; // Replace with your MySQL username
            $password = ""; // Replace with your MySQL password
            $dbname = "testforlab"; // Replace with your MySQL database name

            // Create a connection
            $conn = new mysqli($servername, $username, $password, $dbname);

            // Check the connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            // Prepare a SQL statement to delete the student record
            // Prepare a SQL statement to delete the student record with the provided ID
                    $sql = "DELETE FROM databasestudent WHERE student_id = ?";

//              ///////////////HERE IS SOME PROBLEm
            // Prepare and bind parameters
            $stmt = $conn->prepare($sql);
             $stmt->bind_param("i", $studentID);
            // Execute the statement
            if ($stmt->execute()) {
                // If deletion is successful
                echo "Student deleted successfully.";
            } else {
                // If an error occurred during deletion
                echo "Error deleting student: " . $conn->error;
            }

            // Close the statement and connection
            $stmt->close();
            $conn->close();
        } else {
            // If student ID is not provided
            echo "Student ID is missing.";
        }
    }


    /////// here will be method for get the students when site started
}

?>
