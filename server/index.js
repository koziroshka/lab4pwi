    import express from 'express';
    import { Server } from "socket.io"
    import path from 'path'
    import { fileURLToPath } from 'url'
    import { MongoClient, ServerApiVersion } from 'mongodb';

    const uri = "mongodb+srv://vihovaneckostik:kostya271220A1@cluster0.hze0qob.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0";

    // Create a MongoClient with a MongoClientOptions object to set the Stable API version

    const __filename = fileURLToPath(import.meta.url)
    const __dirname = path.dirname(__filename)


    const PORT = process.env.PORT || 3500
    const ADMIN = "Admin"


    const app = express()


    app.use(express.static(path.join(__dirname, "D:\\xamppServerTwo\\htdocs\\lab4pwi\\server\\public")))

    const expressServer = app.listen(PORT, () => {
        console.log(`listening on port ${PORT}`)
    })

    // state
    const UsersState = {
        users: [],
        setUsers: function (newUsersArray) {
            this.users = newUsersArray
        }
    }




    async function sendDataToChat(data, collectionSelect) {
        const client = new MongoClient(uri);

        try {
            // Connect the client to the MongoDB server
            await client.connect();

            // Access the studentDB database
            const database = client.db("studentDB");

            // Access the chatTemp collection
            const collection = database.collection(collectionSelect);

            // Insert the data into the chatTemp collection
            const result = await collection.insertOne(data);

            console.log(`Inserted ${result.insertedCount} document into the chatTemp collection.`);
        } catch (error) {
            console.error("Error sending data to MongoDB:", error);
        } finally {
            // Close the client
            await client.close();
        }
    }

    // deleteDocumentsByFieldValue("Kostya", "rooms", "a");
    async function deleteDocumentsByFieldValue(collectionName, fieldName, valueToDelete) {
        const client = new MongoClient(uri);

        try {
            await client.connect();

            const database = client.db("usersDB");
            const collection = database.collection(collectionName);

            // Construct the filter object
            const filter = { [fieldName]: valueToDelete };

            // Perform the delete operation
            const result = await collection.deleteMany(filter);

            console.log(`${result.deletedCount} document(s) deleted successfully.`);
        } catch (error) {
            console.error("Error deleting documents:", error);
        } finally {
            await client.close();
        }
    }


    async function checkIfExistInCollectionTheDocument(data, colectionSelect){
        const client = new MongoClient(uri);

        try {
            await client.connect();
            const database = client.db("usersDB");
            const collection = database.collection(collectionSelect);

            // Check if a document with the specified rooms value exists
            const existingData = await collection.findOne({ rooms: data });
            if (existingData) {

                console.log(`Document with rooms ${data} found in collection ${collectionSelect}`);

                return true; // Document exists
            } else {
                console.log(`Document with rooms ${data} not found in collection ${collectionSelect}`);

                return false;
            }
        } catch (error) {
            console.error("Error checking data existence:", error);
            throw error;
        } finally {
            await client.close();
        }


    }


    async function editUserRooms(data, collectionSelect) {
        const client = new MongoClient(uri);

        try {
            await client.connect();
            const database = client.db("usersDB");
            const collection = database.collection(collectionSelect);

            // Check if a document with the specified rooms value exists
            const existingData = await collection.findOne({ rooms: data });

            if (existingData) {
                console.log(`Document with rooms ${data} found in collection ${collectionSelect}`);



                return true; // Document exists
            } else {
                console.log(`Document with rooms ${data} not found in collection ${collectionSelect}`);
                await collection.insertOne({ rooms: data });

                return false; // Document does not exist
            }
        } catch (error) {
            console.error("Error checking data existence:", error);
            throw error;
        } finally {
            await client.close();
        }
    }



    async function createCollaction(name, nameofRoom) {
        const client = new MongoClient(uri);

        try {
            // Connect the client to the MongoDB server
            await client.connect();

            // Access the studentDB database
            const database = client.db(name);

            await database.createCollection(nameofRoom)



            console.log(`Inserted document into the chatTemp collection.`);
        } catch (error) {
            console.error("Error sending data to MongoDB:", error);
        } finally {
            // Close the client
            await client.close();
        }
    }

    async function getAllDocumentsFromCollection(collectionName, nameDS) {
        const client = new MongoClient(uri);
        try {
            await client.connect();
            const database = client.db(nameDS);
            const collection = database.collection(collectionName);
            return await collection.find({}).toArray();
        } finally {
            await client.close();
        }
    }

    async function checkCollectionExists(collectionName) {
        const client = new MongoClient(uri);
        try {
            await client.connect();
            const database = client.db("studentDB");
            const collections = await database.listCollections().toArray();
            return collections.some(collection => collection.name === collectionName);
        } finally {
            await client.close();
        }
    }


    async function checkUserExists(collectionName) {
        const client = new MongoClient(uri);
        try {
            await client.connect();
            const database = client.db("usersDB");
            const collections = await database.listCollections().toArray();
            return collections.some(collection => collection.name === collectionName);
        } finally {
            await client.close();
        }
    }


    async function getAllActiveRoomsFromDataBasse() {
        var documents

        documents = getAllDocumentsFromCollection("Kostya", "usersDB");

        console.log("Room exists in db"  + JSON.stringify(documents));
        /*return Array.from(new Set(UsersState.users.map(user => user.room)))*/
    }





    const io = new Server(expressServer, {
        cors: {
            origin: process.env.NODE_ENV === "production" ? false : ["http://localhost:8080", "http://127.0.0.1:8080"]
        }
    })

    var userName;
    io.on('connection', async  socket => {
        console.log(`User ${socket.id} connected`)

        // Step 1: Receive the user's name
        socket.on('sendTheName', async ({ name }) => {
            console.log(`Received name from user: ${name}`);

            try {
                // Step 2: Retrieve documents from the database
                const documents = await getAllDocumentsFromCollection(name, "usersDB");
                console.log("Room exists in db: " + JSON.stringify(documents));

                // Step 3: Process the retrieved documents
                const rooms = documents.map(doc => doc.rooms);
                const uniqueRooms = Array.from(new Set(rooms));

                console.log("Unique rooms: " + uniqueRooms);

                // Step 4: Emit the room list to the user
                socket.emit('roomList', {
                    rooms: uniqueRooms
                });
            } catch (error) {
                console.error("Error:", error);
            }
        });



        socket.on('enterRoom', async ({ name, room }) => {

            try {
                if (checkUserExists(name)) {

                    createCollaction("usersDB", name)
                    console.log("Create collection for user")

                } else {
                    console.log("User is fined")
                }
            } catch (error) {
                console.error("Error:", error);
            }

            //getAllActiveRoomsFromDataBasse()




            var documents
            try {
                if (await checkCollectionExists(room)) {

                    documents = await getAllDocumentsFromCollection(room, "studentDB");
                    console.log("Room exists in db");

                } else {
                    console.log("Room does not exist in db");
                }
            } catch (error) {
                console.error("Error:", error);
            }

            console.log("Here: " + JSON.stringify(documents));



// Upon connection - only to user
            socket.emit('message', buildMsg(ADMIN, "Welcome to Chat App!"))




            // leave previous room
            const prevRoom = getUser(socket.id)?.room

            if (prevRoom) {
                socket.leave(prevRoom)
                io.to(prevRoom).emit('message', buildMsg(ADMIN, `${name} has left the room`))
            }

            const user = activateUser(socket.id, name, room)

            // Cannot update previous room users list until after the state update in activate user

            if (prevRoom) {

                io.to(prevRoom).emit('userList', {
                    users: getUsersInRoom(prevRoom)
                })

            }


            checkCollectionExists(room).then(exists => {
                if (exists) {
                    console.log("-----------------------------")
                    console.log('Data exists.');
                    console.log("-----------------------------")

                } else {
                    console.log("-----------------------------")

                    createCollaction("studentDB", room).catch(console.error)
                    console.log('Data does not exist.');
                    console.log("-----------------------------")

                }
            })
                .catch(error => {
                    console.error('An error occurred:', error);
                });


            const data = {
                rooms: room
            };


            const roomsValue = data.rooms;


            editUserRooms(roomsValue, name)
                .then(exists => {
                    if (exists) {
                        console.log('Data exists.');
                    } else {
                        console.log('Data does not exist.');
                    }
                })
                .catch(error => {
                    console.error('An error occurred:', error);
                });







            checkCollectionExists(room).then(exists => {
                if (exists) {
                    console.log("-----------------------------")
                    console.log('Data exists.');
                    socket.emit('clearChat');

                    socket.emit('chatHistory', JSON.stringify(documents));

                    console.log("-----------------------------")

                } else {
                    console.log("-----------------------------")

                    createCollaction("studentDB", room).catch(console.error)
                    console.log('Data does not exist.');
                    console.log("-----------------------------")

                }
            })
                .catch(error => {
                    console.error('An error occurred:', error);
                });



            // join room
            socket.join(user.room)

            // To user who joined
            socket.emit('message', buildMsg(ADMIN, `You have joined the ${user.room} chat room`))

            // To everyone else
            socket.broadcast.to(user.room).emit('message', buildMsg(ADMIN, `${user.name} has joined the room`))

            // Update user list for room
            io.to(user.room).emit('userList', {
                users: getUsersInRoom(user.room)
            })

            var documents2

            documents2 =   await  getAllDocumentsFromCollection(name, "usersDB");

            console.log("Room exists in db"  + JSON.stringify(documents2));

            const rooms = documents2.map(doc => doc.rooms);
// Create array with unique values
            const uniqueRooms = Array.from(new Set(rooms));

            console.log(uniqueRooms);
            // Update rooms list for everyone
            socket.emit('roomList', {
                rooms: getAllActiveRooms(uniqueRooms)
            })


        })



        // When user disconnects - to all others
        socket.on('disconnect', () => {
            const user = getUser(socket.id)
            userLeavesApp(socket.id)



            if (user) {
                io.to(user.room).emit('message', buildMsg(ADMIN, `${user.name} has left the room`))

                io.to(user.room).emit('userList', {
                    users: getUsersInRoom(user.room)
                })

                io.emit('roomList', {
                    rooms: getAllActiveRooms()
                })
            }

            console.log(`User ${socket.id} disconnected`)
        })

        // Listening for a message event
        socket.on('message', ({ name, text }) => {
            const room = getUser(socket.id)?.room;
            if (room) {
                // Build the message object
                const message = buildMsg(name, text);


                console.log("--------------------------------------");


                var data = {
                    name: name,
                    text: text,
                    time: message.time
                }


                sendDataToChat(data, room)



                // Emit the message to all clients in the room
                io.to(room).emit('message', message);
            }
        });





        // Listen for activity
        socket.on('activity', (name) => {
            const room = getUser(socket.id)?.room
            if (room) {
                socket.broadcast.to(room).emit('activity', name)
            }
        })
    })

    function buildMsg(name, text) {
        return {
            name,
            text,
            time: new Intl.DateTimeFormat('default', {
                hour: 'numeric',
                minute: 'numeric',
                second: 'numeric'
            }).format(new Date())
        }
    }

    // User functions
    function activateUser(id, name, room) {
        const user = { id, name, room }
        UsersState.setUsers([
            ...UsersState.users.filter(user => user.id !== id),
            user
        ])
        return user
    }

    function userLeavesApp(id) {
        UsersState.setUsers(
            UsersState.users.filter(user => user.id !== id)
        )
    }

    function getUser(id) {
        return UsersState.users.find(user => user.id === id)
    }

    function getUsersInRoom(room) {
        return UsersState.users.filter(user => user.room === room)
    }

    function getAllActiveRooms(room) {
        return room;
    }









    function getAllActiveRoomsForUser(name) {
        // Filter the users to get the rooms where the specified user is present
        const rooms = UsersState.users.filter(user => user.name === name).map(user => user.room);

        // Remove duplicates and return the unique rooms
        return Array.from(new Set(rooms));
    }

