



const socket = io('ws://localhost:3500');





const msgInput = document.querySelector('#message')
const nameInput = document.querySelector('#name')
const chatRoom = document.querySelector('#room')
const activity = document.querySelector('.activity')
const usersList = document.querySelector('.user-list')
const roomList = document.querySelector('.room-list')
const chatDisplay = document.querySelector('.chat-display')



window.addEventListener('DOMContentLoaded', function() {
    var urlParams = new URLSearchParams(window.location.search);
    var username = urlParams.get('username');
    var nameInput = document.getElementById('name');

    if (username) {

        socket.emit('sendTheName', {
            name: username
        })


        nameInput.value = username; // Set the value of the hidden input field
        nameInput.setAttribute('readonly', 'readonly'); // Set readonly attribute
    } else {
    }
});



function sendMessage(e) {
    e.preventDefault()
    if (nameInput.value && msgInput.value && chatRoom.value) {
        socket.emit('message', {
            name: nameInput.value,
            text: msgInput.value
        })
        msgInput.value = ""
    }
    msgInput.focus()
}

function enterRoom(e) {
    e.preventDefault()
    if (nameInput.value && chatRoom.value) {
        socket.emit('enterRoom', {
            name: nameInput.value,
            room: chatRoom.value
        })


    }
}







document.querySelector('.form-msg')
    .addEventListener('submit', sendMessage)

document.querySelector('.form-join')
    .addEventListener('submit', enterRoom)



msgInput.addEventListener('keypress', () => {
    socket.emit('activity', nameInput.value)
})


socket.on( "message", ( data ) => {
    /*activity.textContent = ""*/
    const { name, text, time } = data
    const li = document.createElement( 'li' )
    li.className = 'post'
    if ( name === nameInput.value ) li.className = 'post post--left'
    if ( name !== nameInput.value && name !== 'Admin' ) li.className = 'post post--right'
    if ( name !== 'Admin' ) {
        li.innerHTML = `<div class="post__header ${ name === nameInput.value
            ? 'post__header--user'
            : 'post__header--reply'
        }">
        <span class="post__header--name">${ name }</span> 
        <span class="post__header--time">${ time }</span> 
        </div>
        <div class="post__text">${ text }</div>`
    } else {
        li.innerHTML = `<div class="post__text">${ text }</div>`
    }
    document.querySelector( '.chat-display' ).appendChild( li )

    chatDisplay.scrollTop = chatDisplay.scrollHeight
} )


socket.on('clearChat', () => {
    // Select the chat display element

    console.log("hello from function clearChat")

    const chatDisplay = document.querySelector('.chat-display');

    // Clear the content of the chat display element
    chatDisplay.innerHTML = '';
});


socket.on('chatHistory', (jsonString) => {

    if (jsonString === null) {
        console.log("Received null jsonString. Aborting chat history processing.");
        return; // Stop execution of the function
    }


    const chatDisplay = document.querySelector('.chat-display');

    // Parse the JSON string into a JavaScript object array
    const chatHistory = JSON.parse(jsonString);
    console.log("hello")

    chatHistory.forEach(({ name, text, time }) => {
        const li = document.createElement('li');
        li.className = 'post';

        // Determine the alignment of the message based on the sender's name
        if (name === nameInput.value) {
            li.classList.add('post--left');
        } else {
            li.classList.add('post--right');
        }

        // Create the message HTML structure
        if (name !== 'Admin') {
            const headerClass = name === nameInput.value ? 'post__header--user' : 'post__header--reply';
            li.innerHTML = `
                <div class="post__header ${headerClass}">
                    <span class="post__header--name">${name}</span>
                    <span class="post__header--time">${time}</span>
                </div>
                <div class="post__text">${text}</div>
            `;
        } else {
            li.innerHTML = `<div class="post__text">${text}</div>`;
        }

        chatDisplay.appendChild(li);
    });

    // Scroll to the bottom of the chat display
    chatDisplay.scrollTop = chatDisplay.scrollHeight;

    console.log("Chat history loaded successfully");
});





let activityTimer
socket.on("activity", (name) => {
    activity.textContent = `${name} is typing...`

    // Clear after 3 seconds
    clearTimeout(activityTimer)
    activityTimer = setTimeout(() => {
        activity.textContent = ""
    }, 3000)
})

socket.on('userList', ({ users }) => {
    showUsers(users)
})

socket.on('roomList', ({ rooms }) => {
    showRooms(rooms)
})

function showUsers(users) {
    usersList.textContent = ''
    if (users) {
        usersList.innerHTML = `<em>Users in ${chatRoom.value}:</em>`
        users.forEach((user, i) => {
            usersList.textContent += ` ${user.name}`
            if (users.length > 1 && i !== users.length - 1) {
                usersList.textContent += ","
            }
        })
    }
}

function enterRoomSelect(room) {
    const name = nameInput.value;
    console.log("from the enterRoomSelect");
    if (name && room) {
        // Set the value of the room input field to the selected room
        chatRoom.value = room;

        console.log("Sending enterRoom event for room:", room);
        socket.emit('enterRoom', {
            name: name,
            room: room
        });
    }
}

function showRooms(rooms) {
    roomList.innerHTML = ''; // Clear the room list
    if (rooms) {
        const roomListTitle = document.createElement('em');
        roomListTitle.textContent = 'Active Rooms:';
        roomList.appendChild(roomListTitle);
        roomList.appendChild(document.createElement('br')); // Add line break after the title

        rooms.forEach(room => {
            const roomElement = document.createElement('span');
            roomElement.textContent = room;
            roomElement.style.cursor = 'pointer';
            roomElement.addEventListener('click', () => enterRoomSelect(room));
            roomList.appendChild(roomElement);
            roomList.appendChild(document.createElement('br')); // Add line break after each room
        });
    }
}






// Function to apply hover effect to an element
function applyHoverEffect(elementId) {
    const element = document.getElementById(elementId);
    document.addEventListener('mousemove', (event) => {
        const isHovering = event.target === element || element.contains(event.target);
        element.style.backgroundColor = isHovering ? 'lightblue' : '';
    });
}

// Apply hover effect to room input field
applyHoverEffect('room');

// Apply hover effect to submit button
applyHoverEffect('join');

// Apply hover effect to send button
applyHoverEffect('Sent');