let students = [];
let amountOfStudent = 0;
let generalCurrent =0;
let generalDelete = 0;
let studentConstructorList = [];
let modeOfAddDelete = 0;
let addMode = 0;


let nameOfUser = null;

class StudentClass {
    constructor(group, firstName, lastName, gender, birthday, idStudent) {
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.birthday = birthday;

        this.idStudent = idStudent
    }
}

///
/// Це буде для view так я дані отрмаються з моделі та оноплюююють табличку, тобто реалізовується паттерн
/// addRow() - контроллер -> відправляєтьс до моделі -> модель обробляє
// -> відправляє на view дані про табличку -> ця функція оновлює дані
///
function addTableRowRefresh(studentData) {
    const tableBody = document.getElementById('table-body');

    const row = document.createElement('tr');

    const checkboxCell = document.createElement('td');
    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkboxCell.appendChild(checkbox);
    row.appendChild(checkboxCell);

    const gradeCell = document.createElement('td');
    gradeCell.textContent = studentData.grade;
    row.appendChild(gradeCell);

    const nameCell = document.createElement('td');
    nameCell.textContent = studentData.firstName + " " + studentData.lastName; // Concatenate name and surname
    row.appendChild(nameCell);

    const sexStatus = document.createElement('td');
    sexStatus.textContent = studentData.gender;
    row.appendChild(sexStatus);

    const ageCell = document.createElement('td');
    ageCell.textContent = studentData.age;
    row.appendChild(ageCell);

    const circleCell = document.createElement('td');
    const circle = document.createElement('div');
    circle.style.width = '10px'; // Set width of the circle
    circle.style.height = '10px'; // Set height of the circle
    circle.style.borderRadius = '50%'; // Make it a circle
    const random = Math.random();

    // If random is less than 0.5, set background color to red, otherwise blue
    const color = random < 0.5 ? 'red' : 'green';

    circle.style.backgroundColor = color; // Set color to red
    circleCell.appendChild(circle);
    row.appendChild(circleCell);

    DeleteDiv = row.querySelector('.divForEditDeleteObjects'); // Get existing div

    // Check if div exists (optional, handles cases where no existing div)
    if (!DeleteDiv) {
        DeleteDiv = document.createElement('div');
        DeleteDiv.classList.add('divForEditDeleteObjects');
        row.appendChild(DeleteDiv);
    }

    // Create the open modal button (if not already present)
    let openModalButton = DeleteDiv.querySelector('.openModal');
    if (!openModalButton) {
        openModalButton = document.createElement('button');
        openModalButton.classList.add('openModal');
        openModalButton.id = 'student' + studentData.idStudent;
        openModalButton.onclick = function() {
            generalCurrent = openModalButton.id;
            generalDelete = openModalButton.id;
            deleteModalOpen(generalCurrent);
            //deleteRowByIndex(generalCurrent);
        }

        const deleteIcon = document.createElement('img');
        deleteIcon.src = 'delete.png';
        deleteIcon.alt = 'Delete';
        deleteIcon.style.width = '10px';
        deleteIcon.style.height = '10px';

        openModalButton.appendChild(deleteIcon);
        DeleteDiv.appendChild(openModalButton);
    }

    const editDeleteDiv = document.createElement('div');
    editDeleteDiv.classList.add('divForEditDeleteObjects');

    // Create the edit modal button
    const editModalButton = document.createElement('button');
    editModalButton.classList.add('openModal');
    editModalButton.id = 'studentEdit' + studentData.idStudent;

    editModalButton.onclick = function() {
        generalCurrent = openModalButton.id;
        generalDelete = openModalButton.id;
        modeOfAddDelete = 1;
        switchToEdit(true);
        openAddModalLast();
    }; // Pass student data to function

    // Create the edit icon
    const editIcon = document.createElement('img');
    editIcon.src = 'draw.png';
    editIcon.alt = 'Edit';
    editIcon.style.width = '10px';
    editIcon.style.height = '10px';

    // Add the icon to the button and the button to the div
    editModalButton.appendChild(editIcon);
    editDeleteDiv.appendChild(editModalButton);

    row.appendChild(editDeleteDiv);

    tableBody.appendChild(row);
}

function addTableRow(studentData) {
    const tableBody = document.getElementById('table-body');

    const row = document.createElement('tr');

    const checkboxCell = document.createElement('td');
    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkboxCell.appendChild(checkbox);
    row.appendChild(checkboxCell);

    const gradeCell = document.createElement('td');
    gradeCell.textContent = studentData.grade;
    row.appendChild(gradeCell);

    const nameCell = document.createElement('td');
    nameCell.textContent = studentData.name + " " + studentData.surname; // Concatenate name and surname
    row.appendChild(nameCell);

    const sexStatus = document.createElement('td');
    sexStatus.textContent = studentData.sex;
    row.appendChild(sexStatus);

    const ageCell = document.createElement('td');
    ageCell.textContent = studentData.age;
    row.appendChild(ageCell);

    const circleCell = document.createElement('td');
    const circle = document.createElement('div');
    circle.style.width = '10px'; // Set width of the circle
    circle.style.height = '10px'; // Set height of the circle
    circle.style.borderRadius = '50%'; // Make it a circle
    const random = Math.random();

    // If random is less than 0.5, set background color to red, otherwise blue
    const color = random < 0.5 ? 'red' : 'green';

    circle.style.backgroundColor = color; // Set color to red
    circleCell.appendChild(circle);
    row.appendChild(circleCell);

    DeleteDiv = row.querySelector('.divForEditDeleteObjects'); // Get existing div

    // Check if div exists (optional, handles cases where no existing div)
    if (!DeleteDiv) {
        DeleteDiv = document.createElement('div');
        DeleteDiv.classList.add('divForEditDeleteObjects');
        row.appendChild(DeleteDiv);
    }

    // Create the open modal button (if not already present)
    let openModalButton = DeleteDiv.querySelector('.openModal');
    if (!openModalButton) {
        openModalButton = document.createElement('button');
        openModalButton.classList.add('openModal');
        openModalButton.style.marginLeft = '10px';

        openModalButton.id = 'student' + amountOfStudent;
        openModalButton.onclick = function() {
            generalCurrent = openModalButton.id;
            generalDelete = openModalButton.id;
            deleteModalOpen(generalCurrent);
            //deleteRowByIndex(generalCurrent);
        }

        const deleteIcon = document.createElement('img');
        deleteIcon.src = 'delete.png';
        deleteIcon.alt = 'Delete';
        deleteIcon.style.width = '10px';
        deleteIcon.style.height = '10px';

        openModalButton.appendChild(deleteIcon);
        DeleteDiv.appendChild(openModalButton);
    }

    const editDeleteDiv = document.createElement('div');
    editDeleteDiv.classList.add('divForEditDeleteObjects');

    // Create the edit modal button
    const editModalButton = document.createElement('button');
    editModalButton.classList.add('openModal');
    editModalButton.id = 'studentEdit' + amountOfStudent;
    editModalButton.style.marginLeft = '10px';
    editModalButton.onclick = function() {
        generalCurrent = openModalButton.id;
        generalDelete = openModalButton.id;
        modeOfAddDelete = 1;
        switchToEdit(true);
        openAddModalLast();
    }; // Pass student data to function

    // Create the edit icon
    const editIcon = document.createElement('img');
    editIcon.src = 'draw.png';
    editIcon.alt = 'Edit';
    editIcon.style.width = '10px';
    editIcon.style.height = '10px';

    // Add the icon to the button and the button to the div
    editModalButton.appendChild(editIcon);
    editDeleteDiv.appendChild(editModalButton);

    row.appendChild(editDeleteDiv);
//
    tableBody.appendChild(row);
}


function clearTable() {
    const table = document.getElementById('student-table'); // Replace 'your-table-id' with the actual ID of your table
    const rowCount = table.rows.length;

    console.log(rowCount);

    // Iterate over the rows in reverse order and remove them
    for (let i = rowCount - 1; i > 0; i--) {
        table.deleteRow(i);
    }
}

$(document).ready(function() {
    $('#loginButton').click(function() {
        var email = $('#email').val();
        var password = $('#password').val();

        $.ajax({
            url: 'server.php',
            type: 'POST',
            data: {
                action: 'login',
                email: email,
                password: password
            },
            success: function(response) {
                var data = JSON.parse(response);
                if (data.success) {
                    // Redirect to index.html with username as URL parameter
                    window.location.href = 'index.html?username=' + encodeURIComponent(data.username);
                } else {
                    $('#errorMessage').text('Incorrect email or password');
                }
            },
            error: function(xhr, status, error) {
                console.error(xhr.responseText);
            }
        });
    });

    $('#tasksButton').click(function() {
        // Check if nameOfUser is null
        if (nameOfUser === null) {
            // Show an alert message
            alert('Please authenticate to access tasks.');
        } else {
            // Redirect to tasks.html with username as URL parameter
            window.location.href = 'server/public/tasks.html?username=' + encodeURIComponent(nameOfUser);
        }
    });
})

$(document).ready(function() {
    // Get username from URL parameter
    var urlParams = new URLSearchParams(window.location.search);
    var username = urlParams.get('username');
    nameOfUser = username;

    if (username) {
        // Update name_should with the username
        $('#name_should').html('<b>' + username + '</b>');
    }
});





// Call the function with the username







function deleteRowByIndex1(studentID) {
    // Send an AJAX request to server.php with the student ID to delete

    const numericPart = studentID.replace(/^\D+/g, ''); // Extract numeric part from rowIndex



    $.ajax({
            url: 'server.php',
        type: 'POST',
        data: { action: 'delete', studentID: numericPart },
        success: function(response) {
            console.log(response);

            clearTable();
            fetchAllUsersFromServer()
                .then(users => {
                    clearTable();
                    console.log('All users:', users);
                    students.splice(0, students.length); // Clears the array

                    // Add each user to the students array using the StudentClass constructor
                    users.forEach(user => {
                        let student = new StudentClass(user.group_name, user.first_name, user.last_name, user.gender_name, user.birthday_name, user.student_id);
                        students.push(student);
                        console.log('Students array:', student); // Check the students array after adding users
                    });

                    clearTable();
                    resetRowsServer();
                    deleteModalClose();
                })
                .catch(error => {
                    console.error('Error fetching users:', error);
                });
        },
        error: function(xhr, status, error) {
            // Handle errors (if any)
            console.error(xhr.responseText);
        }
    });

}


function deleteRowByAll() {
    const tableBody = $("#student-table tbody");

    // Select all rows except the first one and remove them
    tableBody.find("tr:not(:first)").remove();


    const rowToDelete = $("#" + "student1");

    if (rowToDelete.length) {
        const numericPart = "student1".replace(/^\D+/g, ''); // Extract numeric part from rowIndex

        const indexToRemove = parseInt(numericPart) - 1; // Adjust index to match array index

        // Remove the parent row of the button's parent
        rowToDelete.parent().parent().remove();

        // Remove the corresponding student from the students array
        if (students && students.length > indexToRemove) {
        } else {
            console.error("Student with index " + indexToRemove + " not found in the students array.");
        }
    } else {
        console.error("Row with ID " + rowIndex + " not found.");
    }

    deleteModalClose();
    $("#overlay").hide();

    deleteModalClose();
    $("#overlay").hide();
}
const modal = document.getElementById('modalBox');


function deleteModalOpen() {
    modal.style.display = 'block';
    document.getElementById('overlay').style.display = 'block';
}

function deleteModalClose() {
    modal.style.display = 'none';
}
async function updateStudent(group, firstName, lastName, gender, birthday, studentID) {
    const url = 'server.php';

    // Create an object to hold the data to be sent in the request body
    let data = {
        action: 'update',
        group: group,
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        birthday: birthday,
        studentID: studentID // Fixed parameter name
    };

    // Convert the data object to a JSON string
    let jsonString = JSON.stringify(data);

    // Make the AJAX POST request asynchronously
    try {
        const responseData = await $.ajax({
            type: 'POST',
            url: url,
            data: jsonString,
            contentType: 'application/json'
        });

        console.log('Student information updated successfully:', responseData);
        // Perform additional actions
        clearTable();
        fetchAllUsersFromServer()
            .then(users => {
                students.splice(0, students.length);
                console.log("This of student massive: " + students.length);
                console.log('All users:', users);
                // Add each user to the students array using the StudentClass constructor
                users.forEach(user => {
                    let student = new StudentClass(user.group_name, user.first_name, user.last_name, user.gender_name, user.birthday_name, user.student_id);
                    students.push(student);
                    console.log(student);
                });

                resetRowsServer();
                closeEditModal();
            })
            .catch(error => {
                console.error('Error fetching users:', error);
            });
    } catch (error) {
        console.error('There was a problem with the AJAX request:', error);
    }
}


function editStudent(student, lastNumber) {
    console.log("in function editStudent before trow in function updateStudnt" + lastNumber);

    let temp = "student" + lastNumber.toString();
    document.getElementById('idOfStudentInput').value = lastNumber;


    console.log("last numer: " + temp + " " + "idOfStudentHidden: " + document.getElementById('idOfStudentInput').value)

    updateStudent(student.group, student.firstName, student.lastName, student.gender, student.birthday, document.getElementById('idOfStudentInput').value);

    /*                    constructor(group, firstName, lastName, gender, birthday, idStudent) {*/
}



function resetRowsServer() {



    for (let i = 0; i < students.length; i++) {

        const grade = students[i].group;
        const firstName = students[i].firstName;
        const lastName = students[i].lastName;
        const age = students[i].birthday;
        const idStudent = students[i].idStudent;
        console.log("From reset server: " + idStudent);
        const gender = students[i].gender;

        const studentData = {
            grade,
            firstName,
            lastName,
            age,
            gender,
            idStudent
        };

        addTableRowRefresh(studentData);
    }
    // Hide the overlay
    document.getElementById('overlay').style.display = 'none';
}



function findStudentWithID(studentID) {
    // Define the URL of your server.php file
    var url = 'server.php';

    // Define the parameters to be sent with the GET request
    var params = {
        action: 'findStudentWithID',
        student_id: studentID
    };

    // Construct the URL with parameters
    var queryString = Object.keys(params).map(key => key + '=' + params[key]).join('&');
    var fullURL = url + '?' + queryString;

    // Return a promise that resolves with the fetched data or rejects with an error
    return new Promise((resolve, reject) => {
        // Make a GET request to the server
        fetch(fullURL)
            .then(response => {
                // Check if the response is successful (status code 200)
                if (response.ok) {
                    // Parse the JSON response
                    return response.json();
                } else {
                    // Throw an error if the response is not successful
                    throw new Error('Network response was not ok.');
                }
            })
            .then(data => {
                // Resolve the promise with the data returned from the server
                resolve(data);
            })
            .catch(error => {
                // Reject the promise with the error
                reject(error);
            });
    });
}

//



function switchToEdit(_switch) {
    const formContainer = document.getElementById('add-student-form-container');
    const header = formContainer.querySelector('span:first-child');

    console.log(header.textContent); // Output: For add students

    if(modeOfAddDelete === 0){
        header.innerHTML = "Add Student";
    }
    else if(modeOfAddDelete === 1) {
        header.innerHTML = "Edit student";
    }

    let lastNumber;
    if (typeof generalCurrent === 'string') {
        const matchResult = generalCurrent.match(/\d+$/);
        if (matchResult) {
            lastNumber = parseInt(matchResult[0]);
            console.log("You find student with index: " + lastNumber);
        } else {
            console.error("No numbers found in generalCurrent:", generalCurrent);
        }
    } else {
        console.error("generalCurrent is not a string:", generalCurrent);
    }


    let groupEdit;
    let nameEdit;
    let firstNameEdit;
    let lastNameEdit;
    let genderEdit;
    let birthDatEdit;
    let studentIdEdit;


    findStudentWithID(lastNumber)
        .then(data => {
            // Handle the data returned from the server
            console.log('Student Information:', data);
            // You can perform further actions with the student information here

            groupEdit = data.group_name;
            firstNameEdit = data.first_name;
            lastNameEdit = data.last_name;
            genderEdit = data.gender_name;
            birthDatEdit = data.birthday_name;
            studentIdEdit = data.student_id;


            console.log('///Group:', groupEdit);
            console.log('First Name:', firstNameEdit);
            console.log('Last Name:', lastNameEdit);
            console.log('Gender:', genderEdit);
            console.log('Birth Date:', birthDatEdit);
            console.log('Id student: ', studentIdEdit);

            if (_switch) {


                if(modeOfAddDelete === 0){

                    document.getElementById('actionValue').value = 'add';

                    document.getElementById("groupNameAdd").value = "";
                    document.getElementById("studentNameAdd").value = "";
                    document.getElementById("studentSurnameAdd").value = "";
                    document.getElementById("genderAdd").value = "";
                    document.getElementById("birthdayAdd").value = "";

                }else if(modeOfAddDelete === 1) {

                    document.getElementById('actionValue').value = 'update';


                    document.getElementById("groupNameAdd").value = groupEdit;
                    document.getElementById("studentNameAdd").value = firstNameEdit;
                    document.getElementById("studentSurnameAdd").value = lastNameEdit;
                    document.getElementById("genderAdd").value = genderEdit;
                    document.getElementById("birthdayAdd").value = birthDatEdit;
                }

                let okButton;
                if (modeOfAddDelete === 1) {


                    if(addMode === 1){
                        okButton = document.getElementById("edit-row-btn");

                    }else if(addMode === 0) {

                        okButton = document.getElementById("add-row-btn");
                    }

                    const editButton = document.createElement("button");
                    editButton.textContent = "Edit";
                    editButton.style.float = "right";
                    editButton.style.marginRight = "10px";
                    editButton.id = "edit-row-btn";

                    okButton.innerHTML = "Close edit";
                    const newButton = okButton.cloneNode(true);
                    okButton.parentNode.replaceChild(editButton, okButton);
                    addMode = 1;

                    // Add event listener to the new button
                    editButton.addEventListener('click', function() {
                        // Close the modal box with ID modalBoxAdd


                        let groupEdit =  document.getElementById("groupNameAdd").value

                        let nameEdit = document.getElementById("studentNameAdd").value
                        let firstNameEdit = nameEdit.split(' ')[0];

                        let lastNameEdit = document.getElementById("studentSurnameAdd").value;


                        let genderEdit = document.getElementById("genderAdd").value
                        let birthDatEdit = document.getElementById("birthdayAdd").value;




                        let student = new StudentClass(groupEdit, firstNameEdit, lastNameEdit, genderEdit, birthDatEdit, studentIdEdit);
                        document.getElementById('idOfStudentInput').value = studentIdEdit;


                        console.log("Hello from switch edit(); Action value" + document.getElementById('actionValue').value);

                        editButton.onclick = editStudent(student, studentIdEdit);
                    });


                } else if (modeOfAddDelete === 0) {


                    if(addMode === 1){
                        okButton = document.getElementById("edit-row-btn");

                    }else if(addMode === 0) {
                        okButton = document.getElementById("add-row-btn");
                    }

                    const addButton = document.createElement("button");
                    addButton.textContent = "Add";
                    addButton.style.float = "right";
                    addButton.style.marginRight = "10px";
                    addButton.id = "add-row-btn";
                    addButton.onclick = addRow;

                    okButton.innerHTML = "Add modal";
                    const newButton = okButton.cloneNode(true);
                    okButton.parentNode.replaceChild(addButton, okButton);

                    addMode = 0;
                }



            }



        })
        .catch(error => {
            // Handle errors occurred during the fetch operation
            console.error('There was a problem with the fetch operation:', error);
        });


}



const addRowBtn = document.getElementById('add-row-btn');


function fetchMaxStudentIDFromServer() {
    return new Promise((resolve, reject) => {
        fetch('server.php?action=max_student_id')
            .then(response => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then(data => {
                resolve(data.max_student_id);
            })
            .catch(error => {
                reject(error);
            });
    });
}

///
/// це буде контролер, тому що він приймає дані,що потім переправиити їх до бази даних(тобто моделі)
///
function addRow() {

    const nameInput = document.getElementById('studentNameAdd');
    const surnameInput = document.getElementById('studentSurnameAdd');
    const ageInput = document.getElementById('birthdayAdd');
    const gradeInput = document.getElementById('groupNameAdd');
    const sexInput = document.getElementById('genderAdd');



    const name = nameInput.value.trim();
    const age = ageInput.value.trim();
    const grade = gradeInput.value.trim();
    const surname = surnameInput.value.trim();
    const sex = sexInput.value.trim();

    let newStudentID = 0;


    // Create student data object
    const studentData = { name, age, grade, surname, sex};
    console.log("From the addRow: "  + studentData);
    let studentDataForCon = new StudentClass(grade, name, surname, sex, age, 0);

    document.getElementById('idOfStudentInput').value = studentDataForCon.idStudent;

    studentDataForCon.idStudent = amountOfStudent;


    // Add the student data to the table
    sendDataToServer(studentDataForCon)
        .then(success => {


            fetchMaxStudentIDFromServer()
                .then(maxStudentID => {
                    // Use the maxStudentID to generate the new student ID

                    console.log("Previous id is " + maxStudentID);

                    amountOfStudent = maxStudentID;
                    console.log("Student amount " + amountOfStudent);

                    console.log("Success in send data in addRow();");

                    console.log("after add to table" + amountOfStudent);
                    studentDataForCon.idStudent = amountOfStudent;

                    students.push(studentDataForCon);

                    clearTable();
                    resetRowsServer();
                    console.log("big time");

                    nameInput.value = '';
                    ageInput.value = '';
                    gradeInput.value = '';
                    surnameInput.value = '';
                    ageInput.value = '';
                    sexInput.value = '';

                    closeAddModalLast();
                    deleteModalClose();


                    document.getElementById('overlay').style.display = 'none';


                })
                .catch(error => {
                    console.error('Error fetching maximum student ID:', error);
                });



            // Clear input fields (optional)


        })
        .catch(error => {

            console.log("Some problem in function addRow()");

            return;
        })




}

function sendDataToServer(student) {
    return new Promise((resolve, reject) => {


        let group = student.group;
        let firstName = student.firstName;
        let lastName = student.lastName; // Fix: Use student.lastName here
        let gender = student.gender;
        let birthday = student.birthday;
        let idOfStudent = String(student.idStudent);


        var dataToSend = "group=" + encodeURIComponent(group) + "&firstName=" + encodeURIComponent(firstName) + "&lastName=" + encodeURIComponent(lastName) + "&gender=" + encodeURIComponent(gender) + "&birthday=" + encodeURIComponent(birthday);




        console.log(dataToSend);
        $.ajax({
            type: "POST",
            url: "server.php",
            data: dataToSend,
            success: function (response) {
                console.log(response);
                console.log("Sent.");
                resolve(true); // Resolve with true on success

            },
            error: function (xhr, status, error) {
                console.error("Failed. Error :", error);
                reject(false); // Reject with false on error
                alert(xhr.responseText);
            }
        });

    });
}




const openModalButtons = document.querySelectorAll(".openModal");
const closeModalButton = document.querySelectorAll(".closeModal")



openModalButtons.forEach(item => {
    item.addEventListener('click', function() {
        modal.style.display = 'block';
    });
});



closeModalButton.forEach(item => {
    item.addEventListener('click', function (){
        modal.style.display = 'none';
    });
})

const editModal = document.getElementById('editModal');

function openEditModal() {
    editModal.style.display = 'block';

}

function closeEditModal() {
    editModal.style.display = 'none';
}
function deleteModalClose(){
    modal.style.display = 'none';
}
/*0------------------------------------0 */

document.addEventListener( "DOMContentLoaded", function () {
    const openAddModalButtons = document.querySelectorAll( ".openModal" );
    const closeAddModalButtons = document.querySelectorAll( ".closeModal" );
    const modalAdd = document.getElementById( 'modalBoxAdd' );

    openAddModalButtons.forEach( item => {
        item.addEventListener( 'click', function () {
            document.getElementById( 'overlay' ).style.display = 'block';
            modeOfAddDelete = 0;
            modalAdd.style.display = 'block';
            switchToEdit(true);
        } );
    } );

    closeAddModalButtons.forEach( item => {
        item.addEventListener( 'click', function () {
            modalAdd.style.display = 'none';
            document.getElementById( 'overlay' ).style.display = 'none';
        } );
    } );
} );

const modalAddClose = document.getElementById ( 'modalBoxAdd' );

function closeAddModalLast() {

    modalAddClose.style.display = 'none';
}

function openAddModalLast(){
    modalAddClose.style.display = 'block';
}


